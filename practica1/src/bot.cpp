#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>



int main(void){

	DatosMemCompartida *d;
	int fd;
	void *p;
	//ABRIR FICHERO
	fd=open("BOT",O_RDWR);
	if(fd<0){
		perror("Error apertura fichero BOT");
		close(fd);
		exit(1);
	}
	//PROYECCION EN MEMORIA
	p=(char *)mmap(NULL,sizeof(*(d)),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
	if(p==MAP_FAILED){
		perror("Error al proyectar el fichero\n");
		exit(1);
	}
	close(fd);
	d=(DatosMemCompartida *) p;
	while(1)
	{
		usleep(25000);
		float centro_raqueta;
		centro_raqueta=((d->raqueta1.y1 + d->raqueta1.y2)/2);
		if(centro_raqueta < d->esfera.centro.y)
			d->accion=1;
		else if(centro_raqueta > d->esfera.centro.y)
			d->accion=-1;
		else
			d->accion=0;

		//usleep(25000);
	}
	munmap(p,sizeof(*(d)));
	return 1;
}
