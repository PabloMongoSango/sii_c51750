#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define BUFSIZE 100

int main(void){

int fd,nread;
char buffer[BUFSIZE];
//crear FIFO
unlink("FIFO");
if(mkfifo("FIFO",0777)==0)
	printf("FIFO de logger creado correctamente (Estoy en logger)\n");
else
{
	perror("No puede crearse el fifo de logger\n");
	exit(1);
}
//abrir FIFO
if(fd=open("FIFO",O_RDONLY)<0){
	perror("No puede abrirse el fifo");
	exit(1);
}
else 
	printf("FIFO de logger correctamente abierto (Estoy en logger)\n");
printf("Puntuacion inicial: Jugador1: 0 puntos, Jugador2: 0 puntos\n");

//entrar en un bucle infinito de recepcion de datos con read
while(1){
	if(nread=read(fd,buffer,BUFSIZE)==-1){
		perror("Error lectura de fifo de logger (Estoy en logger)\n");
		exit(1);
	}
	else if(nread==0){
		perror("Has llegado al final de la cadena de logger\n");
		close(fd);
		unlink("FIFO");
		exit(1);
	}
	else if(buffer[0]=='-'){
		printf("%s \n",buffer);
		break;
	}
	else 
		printf("%s \n",buffer);
}
close(fd);
unlink("FIFO");
return 0;
}	
