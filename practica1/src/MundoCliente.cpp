// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	close(fd_cliente);
	unlink("FIFO_CLIENTE");
	close(fd_teclas);
	unlink("FIFO_TECLAS");
	unlink("BOT");
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.Dismin_size(0.075f);
	int i,x,y,nleido;
	char cadena[1000];//buffer donde escribir los datos
	//unsigned char s,w;

	//actualizar los campos de DatosMemCompartida
	d->esfera=esfera;
	d->raqueta1=jugador1;

	//invocar a OnKeyboardDown
	switch(d->accion){
		case 1:
			OnKeyboardDown('w',0,0);
			break;
		case -1:
			OnKeyboardDown('s',0,0);
			break;
		case 0:
			//jugador1.velocidad.y=0;
			break;
		default:
			break;
	}
	
	//Leer los datos de la FIFO_CLIENTE
	char cliente[200];
	nleido=read(fd_cliente,cliente,sizeof(cliente));
	if(nleido==-1){
		perror("Error al leer coordenadas, estoy en MundoCliente\n");
		exit(1);
	}
	else if(nleido==0){
		perror("He llegado al final de la cadena de coordenadas, estoy en MundoCliente");
		exit(1);
	}
	else;

	//Actualizar los atributos con los datos recibidos
	sscanf(cliente,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);

	//Finalizar el juego cuando uno de los jugadores alcance los 3 puntos
	if(puntos1==3||puntos2==3)
		exit(0);	


}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}*/

	//Escribir los datos de la tuberia
	char teclas[200];
	sprintf(teclas,"%c",key);
	write(fd_teclas,teclas,sizeof(teclas));
}

void CMundoCliente::Init()
{
	int fdb;
	//struct stat bstat;
	char *b;
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//crear fichero del tamaño DatosMemCompartida
	fdb=open("BOT",O_RDWR|O_CREAT|O_TRUNC,0666);
	if(fdb<0){
		perror("Error apertura fdb\n");
		close(fdb);
		exit(1);
	}
	write(fdb,&dtm,sizeof(dtm));

	//proyectar fichero
	b=(char *)mmap(NULL,sizeof(dtm),PROT_READ|PROT_WRITE,MAP_SHARED,fdb,0);
	if(b==MAP_FAILED){
		perror("No se puede abrir el fichero de proyeccion\n");
		exit(1);
	}
	//cerrar fdb
	close(fdb);
	//cast
	d=(DatosMemCompartida *)b;
	
	//crear FIFO cliente y abrir en modo lectura
	unlink("FIFO_CLIENTE");
	if(mkfifo("FIFO_CLIENTE",0666)<0){
		perror("No puede crearse el FIFO_CLIENTE\n");
		exit(1);
	}
	else 
		printf("Se ha creado correctamente FIFO_CLIENTE\n");
	if(fd_cliente=open("FIFO_CLIENTE",O_RDONLY)==-1){
		perror("No puede abrirse el FIFO_CLIENTE\n");
		exit(1);
	}
	else
		printf("Se ha abierto correctamente FIFO_CLIENTE\n");
	

	//crear FIFO para el envio de las teclas pulsadas y abrirla en modo escritura
	unlink("FIFO_TECLAS");
	if(mkfifo("FIFO_TECLAS",0666)<0){
		perror("No puede crearse FIFO_TECLAS,estoy en MundoCliente\n");
		exit(1);
	}
	else
		printf("Se ha creado FIFO_TECLAS, estoy en MundoCliente\n");
	if(fd_teclas=open("FIFO_TECLAS",O_WRONLY)<0){
		perror("No puede abrirse FIFO_TECLAS, estoy en MundoCliente\n");
		exit(1);
	}
	else
		printf("Se ha abierto FIFO_TECLAS,estoy en MundoCliente\n");
}
