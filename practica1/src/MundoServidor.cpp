// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <pthread.h>
void* hilo_comandos(void* d);
#include <signal.h>

void signal_handler(int sig)
{
	printf("Signal is %d\n",sig);	
}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	close(fd_logger);//cerrar la tuberia
	unlink("FIFO");
	close(fd_servidor);
	unlink("FIFO_CLIENTE");
	close(fd_teclas);
	unlink("FIFO_TECLAS");
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.Dismin_size(0.075f);
	int i,x,y;
	char cadena[1000];//buffer donde escribir los datos
	//unsigned char s,w;

	//actualizar los campos de DatosMemCompartida
	d->esfera=esfera;
	d->raqueta1=jugador1;

	//invocar a OnKeyboardDown
	switch(d->accion){
		case 1:
			OnKeyboardDown('w',0,0);
			break;
		case -1:
			OnKeyboardDown('s',0,0);
			break;
		case 0:
			//jugador1.velocidad.y=0;
			break;
		default:
			break;
	}
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		//escribir en la cadena
		sprintf(cadena,"Jugador 2 marca 1 punto, lleva un total de %d puntos/n",puntos2);
		//enviar los datos cuando se produzcan puntos por la tuberia
		write(fd_logger,cadena,strlen(cadena)+1);
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		//escribir en la cadena
                sprintf(cadena,"Jugador 1 marca 1 punto, lleva un total de %d puntos/n",puntos1);
                //enviar los datos cuando se produzcan puntos por la tuberia
                write(fd_logger,cadena,strlen(cadena)+1);

	}
	
	

	//Construir la cadena de texto con los datos a enviar
	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	//Enviar los datos por la tuberia con write
	write(fd_servidor,cad,sizeof(cad));

	//Finalizar el juego cuando uno de los jugadores alcance los 3 puntos
	if(puntos1==3||puntos2==3)
		exit(0);
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}*/
}

void CMundoServidor::Init()
{
	int fdb;
	//struct stat bstat;
	char *b;
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//abrir fifo_logger en modo escritura
	fd_logger=open("FIFO",O_WRONLY);
	if(fd_logger==-1){
		perror("No se puede abrir el FIFO que comunica con logger,estoy en MUNDOSERVIDOR\n");
		exit(1);
	}
	else 
		printf("He abierto el fifo de logger para la puntuacion,estoy en MUNDOSERVIDOR\n");


	//Servidor abre fifo en modo escritura para COORDENADAS
	if(fd_servidor=open("FIFO_CLIENTE",O_WRONLY)==-1){
		perror("No puede abrirse FIFO_CLIENTE para las coordenadas. Estoy en MundoServidor\n");
		exit(1);
	}	
	else
		printf("He abierto FIFO_CLIENTE para las COORDENADAS, estoy en MundoServidor\n");

	//           TUBERIA TECLAS
	//abrir tuberia teclas para la comunicacion entre cliente y servidor
	if(fd_teclas=open("FIFO_TECLAS",O_RDONLY)<0){
		perror("No puede abrirse FIFO_TECLAS para comunicarse con el cliente, estoy en MundoServidor\n");
		exit(1);
	}
	else 
		printf("He abierto FIFO_TECLAS, estoy en MundoServidor\n");

	//creacion del THREAD + funcion
	pthread_create(&thid1, NULL, hilo_comandos, this);
	printf("He creado el thread\n");

	//    SEÑALES
	struct sigaction act;
	int estado;
	act.sa_handler=&signal_handler;
	act.sa_flags=SA_RESTART;
	sigaction(SIGINT,&act,NULL);
	sigaction(SIGTERM,&act,NULL);
	sigaction(SIGPIPE,&act,NULL);
	sigaction(SIGUSR1,&act,NULL);
	if(WIFSIGNALED(estado)>0)
		printf("El servidor ha finalizado con valor %d y con la signal %d \n",WEXITSTATUS(estado),WTERMSIG(estado));
	else 
		printf("El servidor no ha finalizado aun\n");

}

void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador()
{
     printf("Estoy en RecibeComandosJugador\n");
     int seguir=1;
     while (seguir==1) {
	    int nleido;
            usleep(10);
            char cadena[100];
            nleido=read(fd_teclas, cadena, sizeof(cadena));
	    if(nleido==-1){
            perror("Error al leer Teclas(Estoy en MundoServidor)\n");
            exit(1);
            }
            unsigned char key;
            sscanf(cadena,"%c",&key);
	    if(cadena[0]=='-') seguir=0;
	    else{
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
	    }
      }
}
