// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////
#pragma once
#ifndef Raqueta_h
#define Raqueta_h
#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
};
#endif
